﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Oefening_28._5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            HoofdPC.AddPrinter(printer1);
            HoofdPC.AddPrinter(printer2);
            HoofdPC.AddPrinter(printer3);
            HoofdPC.AddPrinter(printer4);
        }
        Printer printer1 = new Printer("printer1");
        Printer printer2 = new Printer("printer2");
        Printer printer3 = new Printer("printer3");
        Printer printer4 = new Printer("printer4");
        PC HoofdPC = new PC();


        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (HoofdPC.PrintAf())
                {
                    RefreshGegevens();
                }
                else
                {
                    MessageBox.Show("Er is geen printer beschikbaar", "Error", MessageBoxButton.OK);
                }
            }
            catch
            {
                MessageBox.Show("Error", "Error", MessageBoxButton.OK);
            }
        }

        private void RefreshGegevens()
        {
            ImageUpdate2(printer1, imgPrinter1, tbPrinter1);
            ImageUpdate2(printer2, imgPrinter2, tbPrinter2);
            ImageUpdate2(printer3, imgPrinter3, tbPrinter3);
            ImageUpdate2(printer4, imgPrinter4, tbPrinter4);
        }

        private void btnReset1_Click(object sender, RoutedEventArgs e)
        {
            printer1.Reset();
            //ImageUpdate();
            ImageUpdate2(printer1, imgPrinter1, tbPrinter1);
        }

        private void btnReset2_Click(object sender, RoutedEventArgs e)
        {
            printer2.Reset();
            ImageUpdate();
        }

        private void btnReset3_Click(object sender, RoutedEventArgs e)
        {
            printer3.Reset();
            ImageUpdate();
        }

        private void btnReset4_Click(object sender, RoutedEventArgs e)
        {
            printer4.Reset();
            ImageUpdate();
        }
        public void ImageUpdate()
        {
            if (printer1.Busy == true)
            {
                imgPrinter1.Source = new BitmapImage(new Uri(@"Images\printer_busy.JPG", UriKind.Relative));
            }
            else
            {
                imgPrinter1.Source = new BitmapImage(new Uri(@"Images\printer_notbusy.JPG", UriKind.Relative));
            }
            if (printer2.Busy == true)
            {
                imgPrinter2.Source = new BitmapImage(new Uri(@"Images\printer_busy.JPG", UriKind.Relative));
            }
            else
            {
                imgPrinter2.Source = new BitmapImage(new Uri(@"Images\printer_notbusy.JPG", UriKind.Relative));
            }
            if (printer3.Busy == true)
            {
                imgPrinter3.Source = new BitmapImage(new Uri(@"Images\printer_busy.JPG", UriKind.Relative));
            }
            else
            {
                imgPrinter3.Source = new BitmapImage(new Uri(@"Images\printer_notbusy.JPG", UriKind.Relative));
            }
            if (printer4.Busy == true)
            {
                imgPrinter4.Source = new BitmapImage(new Uri(@"Images\printer_busy.JPG", UriKind.Relative));
            }
            else
            {
                imgPrinter4.Source = new BitmapImage(new Uri(@"Images\printer_notbusy.JPG", UriKind.Relative));
            }
        }
        public void ImageUpdate2(Printer p, Image i, TextBlock t)
        {
            BitmapImage bmp = null;
            t.Text = p.ToString();
            if (p.Busy)
            {
                bmp = new BitmapImage(new Uri(@"Images\printer_busy.JPG", UriKind.Relative));
            }
            else
            {
                bmp = new BitmapImage(new Uri(@"Images\printer_notbusy.JPG", UriKind.Relative));
            }
            i.Source = bmp;

        }
    }
}

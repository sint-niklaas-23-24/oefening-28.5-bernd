﻿using System.Collections.Generic;

namespace Oefening_28._5
{
    internal class PC
    {
        private List<Printer> _printers;
        public PC()
        {
            Printers = new List<Printer>();
        }
        public List<Printer> Printers //nog geen validatie, dus eig onnodig
        {
            get { return _printers; }
            set { _printers = value; }
        }
        public bool PrintAf()
        {
            bool isKlaar = false;
            foreach (Printer printer in Printers)

                if (printer.Busy == false)
                {
                    printer.Busy = true;
                    isKlaar = true;
                    break;
                }

            return isKlaar;
        }
        public void AddPrinter(Printer p)
        {
            _printers.Add(p);
        }
    }
}
